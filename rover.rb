  def plateau_length
    puts "Looking through your telescope you see the plateau your rover will land on. Let's calculate its area. Input one integer representing one side of your plateau:"
    $plateau_length = gets.chomp.to_i
  end

class Rover
	attr_reader :new_x_y

	DIRECTIONS = ["n", "e", "s", "w"]

	def initialize (x = 0, y = 0, direction = "n")
		@x = x
		@y = y
		@direction = direction
	end

	def user_input
		puts "Where do you want to go? Input any combination of M-ove, turn L-eft, or turn R-ight OR type Stay to abandon your rover! "
		@new_x_y = gets.strip.downcase
		@new_x_y.each_char do |i|
			case i
				when "l"
					@direction = DIRECTIONS[DIRECTIONS.index(@direction)-1]
            # => takes the direction array constant and finds the current rover direction index value,
            # => when the rover turns left this cycles through the index in a reverse order and returns
            # => the cardinal direction directly before the original to give the new direction the rover
            # => is now facing
				when "r"
					@direction = DIRECTIONS[(DIRECTIONS.index(@direction)+1)%4]
          # => similar to the above but now we need to loop back around
          # => from the last index to the first when we get to the last item in
          # => the array. Using %4 allows us to loop back to 0 when @direction reaches 3 because (3+1)%4 returns
          # => a new index of 0 and on and on and on and on...yay!
				when "m"
					plateau_boundary
        when "stay"
					break
			end
		end

	end

	def move # => moves the rover based on the direction it faces
		case @direction
		when "n" then @y += 1
		when "s" then @y -= 1
		when "e" then @x += 1
		when "w" then @x -= 1
    end
  end

	  # =>  the turns are now accounted for in the user_input method so we don't need this one
  # def turn
  # end
  def plateau_boundary
    if @x <= $plateau_length && @y <= $plateau_length
      move
    else
      puts "Oh no! Your rover has fallen to an untimely demise! Kersplat..."
      exit
    end
  end




	def to_s
		"#{@x}, #{@y} facing => #{@direction}"
  end

end

rover = Rover.new

plateau_length

while true
  rover.user_input
	puts rover

	if rover.new_x_y == "stay"
    puts "Say goodbye to your Rover! If you don't it will think you don't care!"
		exit
	end

end
