class Rover
attr_accessor :x_coor, :y_coor, :direction, :length

  def initialize(x, y, direction, length)
    @x = x
    @y = y
    @direction = direction
    @length = length
  end

  def user_input
    puts "Assuming a square plateau; input your plateau length:"
    @length = gets

    puts "Now add your starting x coordinate for your rover (cannot be higher than #{length}):"
    @x = gets

    puts "And your rover's starting y coordinate (cannot be higher than #{length}):"
    @y = gets

    puts "Which direction is your rover facing? N W E S"
    @direction = gets.chomp.upcase

    puts "Now, where do you want your rover to go today? L to turn left, R to turn right, and M to move forward one grid square. No spaces."
    instructions = gets.chomp.upcase

    puts "#{@length}x#{@length}, #{@x}, #{@y}, #{@direction}, instructions"

  end



  def read_instruction
  end

  def move
  end

  def turn
  end

  def final_position
    puts "#{}"
  end

end

r1 = Rover.new(0,0,0,0)
r1.user_input
