class Person
  attr_accessor :name

  def initialize(name)
    @name = name
  end

  def greeting
    puts "Hi my name is #{name}!"
  end
end

class Student < Person
    def learn
      puts "I get it!"
    end
end

class Instructor < Person
    def teach
      puts "Everything in Ruby is an object!"
    end
end

chris = Instructor.new("Chris")
chris.greeting
chris.teach

christina = Student.new("Christina")
christina.greeting
christina.learn

# => Um for part 8 I can't tell you why it didn't work because when I ran my code it DID work!
# => Did I do it right but wrong? :/
